package com.employee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.model.Employee;
import com.employee.model.Field;
import com.employee.model.Skill;
import com.employee.repository.EmployeeRepository;
import com.employee.repository.FieldRepository;
import com.employee.repository.FieldRepositoryCustom;
import com.employee.repository.SkillRepository;

@Service
public class SkillServiceImpl implements SkillService
{
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private SkillRepository skillRepository;
	
	@Autowired
	private FieldRepository fieldRepository;
	
	@Override
	public List<Field> findAllSkillsByEmployee(long employeeId) 
	{
		try
		{
			Optional<Employee> employeeExist = employeeRepository.findById(employeeId);
			if(employeeExist!=null)
			{
				Optional<Skill> skill =  skillRepository.findById(employeeExist.get().getSkill().getId());
				if(skill!=null)
				{
					return skill.get().getField();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public Skill addSkillToEmployee(long employeeId, Skill skill)
	{
		try
		{
			Optional<Employee> employeeExist = employeeRepository.findByIdAndSkillId(employeeId, skill.getId());
			if(employeeExist!=null)
			{
				Skill skillObj = new Skill();
				skillObj.setId(employeeExist.get().getSkill().getId());
				skillObj.setExperience(employeeExist.get().getSkill().getExperience());
				skillObj.setSummary(employeeExist.get().getSkill().getSummary());
				for(Field field : skill.getField())
				{
					field.setSkill(employeeExist.get().getSkill());
				    fieldRepository.save(field);
				}
				skillObj.setField(fieldRepository.findBySkillId(skill.getId()));
				return skillObj;
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public Optional<Skill> findSkillFromEmployeeById(long employeeId, long skillId) 
	{
		try
		{
			Optional<Employee> employeeExist = employeeRepository.findByIdAndSkillId(employeeId, skillId);
			if(employeeExist!=null)
			{
				return skillRepository.findById(skillId);
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public Skill updateSkillFromEmployeeById(long employeeId, long skillId, Skill skill) 
	{
		try
		{
			Optional<Employee> employeeExist = employeeRepository.findByIdAndSkillId(employeeId, skillId);
			if(employeeExist!=null)
			{
				Skill skillObj = new Skill();
				skillObj.setId(skillId);
				skillObj.setExperience(skill.getExperience());
				skillObj.setSummary(skill.getSummary());
				for(Field field : skill.getField())
				{
					field.setSkill(skill);
				    fieldRepository.save(field);
				}
				return skillRepository.save(skill);
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}	
	}

	@Override
	public Optional<Skill> removeSkillFromEmployeeById(long employeeId, long skillId, Skill skill) 
	{
		try
		{
			Optional<Employee> employeeExist = employeeRepository.findByIdAndSkillId(employeeId, skillId);
			if(employeeExist!=null)
			{
				Skill skillObj = new Skill();
				skillObj.setId(skillId);
				skillObj.setExperience(skill.getExperience());
				skillObj.setSummary(skill.getSummary());
				for(Field field : skill.getField())
				{
					field.setSkill(skill);
				    fieldRepository.delete(field);
				}
				return skillRepository.findById(skillId);
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}	
	}

}
