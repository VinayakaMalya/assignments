package com.employee.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.model.Field;
import com.employee.model.Skill;

@Repository
public interface SkillRepository extends JpaRepository<Skill,Long>, SkillRepositoryCustom
{

}
