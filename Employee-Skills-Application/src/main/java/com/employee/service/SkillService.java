package com.employee.service;

import java.util.List;
import java.util.Optional;

import com.employee.model.Field;
import com.employee.model.Skill;

public interface SkillService 
{
	List<Field> findAllSkillsByEmployee(long employeeId);

	Skill addSkillToEmployee(long employeeId, Skill skill);

	Optional<Skill> findSkillFromEmployeeById(long employeeId, long skillId);

	Skill updateSkillFromEmployeeById(long employeeId, long skillId, Skill skill);

	Optional<Skill> removeSkillFromEmployeeById(long employeeId, long skillId, Skill skill);
}
