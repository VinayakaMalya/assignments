package com.employee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.model.Employee;
import com.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService
{
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	public List<Employee> findAllEmployees() 
	{
		try
		{
			return employeeRepository.findAll();
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public Employee createEmployee(Employee employee) 
	{
		try
		{
			return employeeRepository.save(employee);
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public Optional<Employee> findEmployeeById(long employeeId)
	{
		try
		{
			return employeeRepository.findById(employeeId);
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public Optional<Employee> updateEmployeeById(long employeeId,Employee employee) 
	{
		try
		{
			Optional<Employee> employeeExist = employeeRepository.findById(employeeId);
			if(employeeExist!=null && employeeExist.isPresent())
			{
				Employee emp = new Employee();
				emp.setId(employeeId);
				emp.setFirstName(employee.getFirstName());
				emp.setLastName(employee.getLastName());
				emp.setBirthDate(employee.getBirthDate());
				emp.setCompanyEmail(employee.getCompanyEmail());
				emp.setHiredDate(employee.getHiredDate());
				emp.setRole(employee.getRole());
				emp.setBusinessUnit(employee.getBusinessUnit());
				employeeRepository.save(emp);
				return employeeExist;
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public Optional<Employee> removeEmployeeById(long employeeId) 
	{
		try
		{
			Optional<Employee> employeeExist = employeeRepository.findById(employeeId);
			if(employeeExist!=null)
			{
				employeeRepository.deleteById(employeeExist.get().getId());
				return employeeExist;
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}
	}
}
