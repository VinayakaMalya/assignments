package com.employee.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.employee.model.Employee;
import com.employee.model.Field;
import com.employee.model.Skill;
import com.employee.service.EmployeeService;
import com.employee.service.SkillService;


@RestController
public class EmployeeController 
{
	
	@Autowired
	public EmployeeService employeeService;
	
	@Autowired
	public SkillService skillService;

	/*************************************************EMPLOYEE API**********************************************************/
	
	/*
	 * Get all Perficient employees.
	 * '200': description: Retrieved all Perficient employees.
	 */
	@RequestMapping(value="/employees", method=RequestMethod.GET)
	public List<Employee> findAllEmployees()
	{
		return employeeService.findAllEmployees();
	}
	
	
	/*
	 * Create a Perficient employee.
	 * '201': description: Created new Perficient employee.
	 * '422': description: Invalid Perficient employee data sent to server.  
	 */
	@RequestMapping(value="/employees", method=RequestMethod.POST)
	public Employee createEmployee(@RequestBody Employee employee)
	{
		return employeeService.createEmployee(employee);
	}
	
	
	/*
	 * Find a Perficient employee by ID.
	 * '200': description: Retrieved a Perficient employee.
	 * '400': description: Invalid ID format.
	 * '404': description: Perficient employee not found.
	 */
	@RequestMapping(value="/employees/{employeeId}", method=RequestMethod.GET)
	public Optional<Employee> findEmployeeById(@PathVariable("employeeId") long employeeId)
	{
		return employeeService.findEmployeeById(employeeId);
	}
	
	
	/*
	 * Update a Perficient employee by ID.
	 * '200': Employee Object
	 * '400': description: Invalid ID format.
	 * '404': description: Perficient employee not found.
	 * '422': description: Invalid Perficient employee data sent to server.
	 */ 
	@RequestMapping(value="/employees/{employeeId}", method=RequestMethod.PUT)
	public Optional<Employee> updateEmployeeById(@PathVariable("employeeId") long employeeId,@RequestBody Employee employee)
	{
		return employeeService.updateEmployeeById(employeeId,employee);
	}
	
	
	/*
	 * Update a Perficient employee by ID.
	 * '204': description: Deleted a Perficient employee.
	 * '400': description: Invalid ID format.
	 * '404': description: Perficient employee not found.
	 */ 
	@RequestMapping(value="/employees/{employeeId}", method=RequestMethod.DELETE)
	public Optional<Employee> removeEmployeeById(@PathVariable("employeeId") long employeeId)
	{
		return employeeService.removeEmployeeById(employeeId);
	}
	
	
	/*************************************************SKILL API**********************************************************/
	
	/*
	 * Get all technical skills from a Perficient employee.
	 * '200': description: Retrieved all technical skills from a Perficient employees.
	 */ 
	@RequestMapping(value="/employees/{employeeId}/skills", method=RequestMethod.GET)
	public List<Field> findAllSkillsByEmployee(@PathVariable("employeeId") long employeeId)
	{
		return skillService.findAllSkillsByEmployee(employeeId);
	}
	

	/*
	 * Add a technical skill to a Perficient employee.
	 * '201': description: Created new technical skill to a Perficient employee.
	 * '400': description: Invalid ID format.
     * '404': description: Perficient employee not found.
     * '422': description: Invalid technical skill data sent to server.   
	 */ 
	@RequestMapping(value="/employees/{employeeId}/skills", method=RequestMethod.POST)
	public Skill addSkillToEmployee(@PathVariable("employeeId") long employeeId, @RequestBody Skill skill)
	{
		return skillService.addSkillToEmployee(employeeId,skill);
	}
	
	/*
	 * Find a technical skill, from a Perficient employee, by ID
	 * '200': description: Retrieved all technical skills from a Perficient employees.
	 * '400': description: Invalid ID format.
     * '404': description: Perficient employee not found.
	 */ 
	@RequestMapping(value="/employees/{employeeId}/skills/{skillId}", method=RequestMethod.GET)
	public Optional<Skill> findSkillFromEmployeeById(@PathVariable("employeeId") long employeeId, @PathVariable("skillId") long skillId)
	{
		return skillService.findSkillFromEmployeeById(employeeId,skillId);
	}
	
	/*
	 * 'Update a technical skill, from a Perficient employee, by ID.'
	 * '200': description: Retrieved all technical skills from a Perficient employees.
	 * '400': description: Invalid ID format.
     * '404': description: Perficient employee not found.
     * '422': description: Invalid technical skill data sent to server.
	 */ 
	@RequestMapping(value="/employees/{employeeId}/skills/{skillId}", method=RequestMethod.PUT)
	public Skill updateSkillFromEmployeeById(@PathVariable("employeeId") long employeeId, @PathVariable("skillId") long skillId,@RequestBody Skill skill)
	{
		return skillService.updateSkillFromEmployeeById(employeeId,skillId,skill);
	}
	
	
	/*
	 * 'Delete a technical skill, from a Perficient employee, by ID.'
	 * '204': description: 'Deleted a technical skill, from a Perficient employee, by ID.'
     * '400': description: Invalid ID format.
     * '404': description: Perficient employee not found.
	 */ 
	@RequestMapping(value="/employees/{employeeId}/skills/{skillId}", method=RequestMethod.DELETE)
	public Optional<Skill> removeSkillFromEmployeeById(@PathVariable("employeeId") long employeeId, @PathVariable("skillId") long skillId,@RequestBody Skill skill)
	{
		return skillService.removeSkillFromEmployeeById(employeeId,skillId,skill);
	}
	
	
}
